# Off-Axis-Projection

## Description
Scripts to modify the camera's frustum so it will always look at a specific plane.

## Visuals
![](images/off-axis-projection-1.gif)  

## References
https://medium.com/@michel.brisis/off-axis-projection-in-unity-1572d826541e
https://stackoverflow.com/questions/16416552/achieving-off-axis-projection

## Project status
In development.
