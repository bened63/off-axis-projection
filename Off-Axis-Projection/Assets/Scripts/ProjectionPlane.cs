﻿using UnityEngine;

[ExecuteInEditMode]
public class ProjectionPlane : MonoBehaviour
{
	[SerializeField, Min(0.5f)] private float _width = 1f;
	[SerializeField, Min(0.5f)] private float _height = 1f;

	[SerializeField] private MeshFilter _plane;

	//[SerializeField] private Vector2 _aspectRatio = new Vector2(16, 9);
	[SerializeField] private bool _drawGizmo = true;

	private Vector2 previousSize = new Vector2(8, 4.5f);
	private Vector2 previousAspectRatio = new Vector2(16, 9);

	// Bottom-left, Bottom-right top-left, top-right corners of plane respectively
	public Vector3 bottomLeft { get; private set; }

	public Vector3 bottomRight { get; private set; }
	public Vector3 topLeft { get; private set; }
	public Vector3 topRight { get; private set; }

	// Vector right, up, normal from center of plane
	public Vector3 dirRight { get; private set; }

	public Vector3 dirUp { get; private set; }
	public Vector3 dirNormal { get; private set; }

	private Matrix4x4 _matrix;
	public Matrix4x4 matrix { get => _matrix; }

	private void OnDrawGizmos()
	{
		if (_drawGizmo)
		{
			Gizmos.color = Color.yellow;
			Gizmos.DrawLine(bottomLeft, bottomRight);
			Gizmos.DrawLine(bottomLeft, topLeft);
			Gizmos.DrawLine(topRight, bottomRight);
			Gizmos.DrawLine(topLeft, topRight);

			// Draw direction (front)
			Gizmos.color = Color.white;
			var planeCenter = bottomLeft + ((topRight - bottomLeft) * 0.5f);
			Gizmos.DrawLine(planeCenter, planeCenter + dirNormal);

			float radius = 0.125f;
			Gizmos.color = Color.yellow;
			Gizmos.DrawSphere(bottomLeft, radius);
			Gizmos.color = Color.blue;
			Gizmos.DrawSphere(bottomRight, radius);
			Gizmos.color = Color.red;
			Gizmos.DrawSphere(topLeft, radius);
			Gizmos.color = Color.cyan;
			Gizmos.DrawSphere(topRight, radius);
		}
	}

	private void Start()
	{
	}

	private void Update()
	{
		if (_plane == null)
		{
			_width = Mathf.Max(0.5f, _width);
			_height = Mathf.Max(0.5f, _height);
		}
		else
		{
			bottomLeft = _plane.transform.TransformPoint(_plane.GetComponent<MeshFilter>().sharedMesh.vertices[0]);
			topLeft = _plane.transform.TransformPoint(_plane.GetComponent<MeshFilter>().sharedMesh.vertices[2]);
			topRight = _plane.transform.TransformPoint(_plane.GetComponent<MeshFilter>().sharedMesh.vertices[3]);
			bottomRight = _plane.transform.TransformPoint(_plane.GetComponent<MeshFilter>().sharedMesh.vertices[1]);
			//Debug.Log("<color=lime>"+ _plane.GetComponent<MeshFilter>().sharedMesh.vertexCount + "</color>");
			//Debug.Log("<color=lime>" + btmLeft + "</color>");
			//Debug.Log("<color=lime>" + btmRight + "</color>");
			//Debug.Log("<color=lime>" + topLeft + "</color>");
			//Debug.Log("<color=lime>" + topRight + "</color>");
			_width = Vector3.Distance(topRight, topLeft);
			_height = Vector3.Distance(topLeft, bottomLeft);
		}
		//_aspectRatio.x = Mathf.Max(1, _aspectRatio.x);
		//_aspectRatio.y = Mathf.Max(1, _aspectRatio.y);

		//previousSize = Size;
		//previousAspectRatio = AspectRatio;

		//bottomLeft = transform.TransformPoint(a);
		//bottomRight = transform.TransformPoint(b);
		//topLeft = transform.TransformPoint(c);
		//topRight = transform.TransformPoint(d);

		dirRight = (bottomRight - bottomLeft).normalized;
		dirUp = (topLeft - bottomLeft).normalized;
		dirNormal = -Vector3.Cross(dirRight, dirUp).normalized;

		_matrix = Matrix4x4.zero;
		_matrix[0, 0] = dirRight.x;
		_matrix[0, 1] = dirRight.y;
		_matrix[0, 2] = dirRight.z;

		_matrix[1, 0] = dirUp.x;
		_matrix[1, 1] = dirUp.y;
		_matrix[1, 2] = dirUp.z;

		_matrix[2, 0] = dirNormal.x;
		_matrix[2, 1] = dirNormal.y;
		_matrix[2, 2] = dirNormal.z;

		_matrix[3, 3] = 1.0f;
	}
}