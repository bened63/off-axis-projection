﻿using UnityEngine;

[ExecuteInEditMode]
[RequireComponent(typeof(Camera))]
public class ProjectionCamera : MonoBehaviour
{
	public ProjectionPlane _projectionPlane;
	public bool _drawGizmos = true;
	private Camera _cam;
	private Vector3 va, vb, vc, vd;
	private Vector3 viewDir;
	private Vector3 pos;
	private float _nearClipPlane, _farClipPlane;

	// Extents of perpendicular projection
	private float l, r, b, t;

	private void Awake()
	{
		_cam = GetComponent<Camera>();
	}

	private void OnDrawGizmos()
	{
		if (_projectionPlane == null)
			return;

		if (_drawGizmos)
		{
			var pos = transform.position;
			Gizmos.color = Color.blue;
			Gizmos.DrawLine(pos, pos + va);
			Gizmos.DrawLine(pos, pos + vb);
			Gizmos.DrawLine(pos, pos + vc);
			Gizmos.DrawLine(pos, pos + vd);

			Vector3 pa = _projectionPlane.bottomLeft;
			Vector3 vr = _projectionPlane.dirRight;
			Vector3 vu = _projectionPlane.dirUp;

			Gizmos.color = Color.white;
			Gizmos.DrawLine(pos, viewDir);
		}
	}

	private void LateUpdate()
	{
		if (_projectionPlane != null)
		{
			Vector3 pa = _projectionPlane.bottomLeft;
			Vector3 pb = _projectionPlane.bottomRight;
			Vector3 pc = _projectionPlane.topLeft;
			Vector3 pd = _projectionPlane.topRight;

			Vector3 vr = _projectionPlane.dirRight;
			Vector3 vu = _projectionPlane.dirUp;
			Vector3 vn = _projectionPlane.dirNormal;

			Matrix4x4 matrix = _projectionPlane.matrix;

			pos = transform.position;

			// Camera to projection screen corners
			va = pa - pos;
			vb = pb - pos;
			vc = pc - pos;
			vd = pd - pos;

			viewDir = pos + va + vb + vc + vd;

			// Distance from camera to projection plane
			float d = -Vector3.Dot(va, vn);
			//if (ClampNearPlane)
			//    _cam.nearClipPlane = d;
			_nearClipPlane = _cam.nearClipPlane;
			_farClipPlane = _cam.farClipPlane;

			float nearOverDist = _nearClipPlane / d;
			l = Vector3.Dot(vr, va) * nearOverDist;
			r = Vector3.Dot(vr, vb) * nearOverDist;
			b = Vector3.Dot(vu, va) * nearOverDist;
			t = Vector3.Dot(vu, vc) * nearOverDist;
			Matrix4x4 P = Matrix4x4.Frustum(l, r, b, t, _nearClipPlane, _farClipPlane);

			// Translation to camera position
			Matrix4x4 T = Matrix4x4.Translate(-pos);

			Matrix4x4 R = Matrix4x4.Rotate(Quaternion.Inverse(transform.rotation) * _projectionPlane.transform.rotation);
			_cam.worldToCameraMatrix = matrix * R * T;

			_cam.projectionMatrix = P;
		}
	}
}